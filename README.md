# Ng2Starter


## Before you go on to start understanding this project, please go through these links for once

	-> ES6 -> http://es6-features.org/

	-> Typescript -> https://www.typescriptlang.org/

	-> Angular -> https://www.tutorialspoint.com/angular2/index.htm

	-> Angular-cli -> https://cli.angular.io/

## Understanding the meta files
	
	-> package.json --> place where you define dependencies and much more
	-> tslint.json --> Typescript config for linting
	-> protractor.conf, karma.conf --> for unit testing
	-> angular-cli.json --> this is a scaffolded application so, this is the config file of that generator. It serves few purposes when required.

## Code entry point is ./src/main.ts --> app.module --> everyting follows



## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.






