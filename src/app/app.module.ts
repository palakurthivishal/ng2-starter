/*

  This is the root module for the entire application.
  

  *** Some key points ***
  Import all the required entities of this module, such as
  - components/directives/pipes
  - providers
  - any dependency modules


  'declarations' can register only @Component/@Pipe/@Directive annotated entites
  'providers' can only register @Injectable annotated entity (also known as services)
  'imports' can only register other modules which are annotated with 'NgModule'

  'exports' is a special case, whenever you use a module as a dependency to another module, 
  this propery defines what are all being exported to the other module

  
  For example
    ModuleA has CompA, PipeA, ProviderA
    ModuleB imports ModuleA
    then, the 'exports' property in ModuleA defines what entities you can use in ModuleB

    saying that, anything registered in 'providers' is auto exported so that you need not re-export it once again.
*/



import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core'; // needed to annotate the class
import { FormsModule } from '@angular/forms'; //needed if you support forms or if you use ngModel
import { HttpModule } from '@angular/http'; // necessary for AJAX
import {RouterModule} from '@angular/router'; //necessary for routing since this is a SPA


import { AppComponent } from './app.component'; // this module's root component


import {routing} from './app.routing'; // this module's route configuration file

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    routing
  ],
  providers: [

  ],
  bootstrap : [AppComponent] // you have to specify which is the root component of your app
})
export class AppModule { }
