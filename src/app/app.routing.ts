import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

import {AppComponent} from './app.component';

/*
	For a route to load you can either specify 
	- component
	- path to a module with its name hashed as suffix


	Lazy loaded modules behave in a slightly different fashion

	Lets say ModuleA has some routing config and ModuleB has some routing config
	and ModuleB routes are childs to ModuleA routes in the same way we configured below

	then,
	if you import Bootstrap module in ModuleA then you have to re-import the same module in ModuleB if you are using any bootstrap components

*/


export const routes:Routes = [{
	path : '',
	pathMatch : 'full',
	redirectTo : '/home'
},{
	path : 'home', // this path will be lazily loaded
	loadChildren : './prelogin/prelogin.module#PreLoginModule'
}];

export const routing:ModuleWithProviders = RouterModule.forRoot(routes); // since this the root routing file, we have used 'forRoot'