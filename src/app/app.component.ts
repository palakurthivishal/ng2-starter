/*
	This is the root component of the application which will be bootstrapped when app loads

	All its child route's template falls into 
	this component's template onto <router-outlet></router-outlet>
	specifying this tag is mandatory

	For example,
	Prelogin and Dashboard module route's templates fall into this template
*/


import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', // need to specify the relative URL of the template file
  styleUrls: ['./app.component.css'] // need to specify the relative URL of the styles file
})
export class AppComponent {
  title = 'app works!';
}
