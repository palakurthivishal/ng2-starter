import {NgModule} from '@angular/core';


import {LandingComponent} from './components/landing/landing.component';

import {routing} from './prelogin.routing';


@NgModule({
	declarations : [
		LandingComponent
	],
	imports : [
		routing
	]
})
export class PreLoginModule{

}