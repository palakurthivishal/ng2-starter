import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

import {LandingComponent} from './components/landing/landing.component';

export const routes:Routes = [{
	path : '',
	pathMatch : 'full',
	redirectTo : '/home/landing'
},{
	path : 'landing',
	component : LandingComponent
}];

export const routing:ModuleWithProviders = RouterModule.forChild(routes); // notice the difference here with 'forChild'